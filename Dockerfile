FROM node:17-alpine

LABEL MAINTAINER="nanyuantingfeng@aliyun.com"

COPY dist dist

COPY package.json .

ENV NPM_CONFIG_LOGLEVEL warn

# npm config set registry https://registry.npm.taobao.org/ &&
RUN npm i --production

RUN ls -al -R

EXPOSE 59999/udp

CMD [ "node" , "dist/server.js" ]
