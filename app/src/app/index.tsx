import * as React from 'react';

import {LWM2MConnect} from '@hollowy/lwm2m'
import { useEffect, useState } from 'react'
import "antd/dist/antd.css"

import { Button, Input, message, Select } from 'antd'

const connect = new LWM2MConnect(59999, window.location.hostname)

export function App() {
  const [nodes, setNodes] = useState<any>([])
  const [value, setValue] = useState<string>()
  const [clientName, setClientName] =useState<string>()

  useEffect(() => {
    connect.list().then(data => {
      console.log(data)
      setNodes(data)
    })

    connect.subscribe("device::notify", (clientName:string, data: any) => {
      console.log("device::notify",clientName, data)
    })

    connect.subscribe("device::status", (clientName: string, status) => {
      console.log("device::status",clientName, status)
      message.info("设状态变更:" + clientName+":" + status)
    })

  }, [])


  const distributed = () => {
    console.log(clientName,value)
    connect.write(clientName!, "/19/1/0", value, true).catch(e => {
      console.log("ERROR",e)
    })
  }



  return (
      <div>
        {
          nodes.map(d => {
            return (<div key={d.clientName}>{JSON.stringify(d)}</div>)
          })
        }

        <hr/>
        <hr/>

        <div style={{display : "flex"}}>
          <div style={{width : 200, textAlign : "right"}}>数据下发</div>
          <div>
            <Select style={{ width: 220 }} onChange={setClientName}>
              {
                nodes.map(d => {
                  return (
                    <Select.Option key={d.clientName} value={d.clientName}>{
                      d.clientName
                    }</Select.Option>
                  )
                })
              }
            </Select>
          </div>
          <div style={{width : 400}}>
            <Input.TextArea rows={4} placeholder={"JSON"}
                            value={value}
                            onChange={(e) => setValue(e.target.value)}  />
          </div>

          <Button onClick={distributed}>下发</Button>
        </div>
      </div>
  );
}
