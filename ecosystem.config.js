module.exports = {
  apps: [
    {
      script: "dist/server.js",
      watch: "dist",
      cwd: ".",
      instances: 1,
      error_file: "./logs/coap.error.log",
      out_file: "./logs/coap.log",
      log_date_format: "YYYY-MM-DD HH:mm Z",
    },
    {
      script: "dist/app.js",
      watch: "dist",
      cwd: ".",
      instances: 1,
      error_file: "./logs/app.error.log",
      out_file: "./logs/app.log",
      log_date_format: "YYYY-MM-DD HH:mm Z",
    },
  ],
};
