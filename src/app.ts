/***************************************************
 * Created by nanyuantingfeng on 2022/4/3 20:27. *
 ***************************************************/
const Koa = require('koa')
const path = require('path')
const static = require('koa-static')

const app = new Koa()

// 静态资源目录对于相对入口文件index.js的路径
const STATIC_PATH = path.join(__dirname, '../app/build')

app.use(static(STATIC_PATH))

app.use(async (ctx) => {
  ctx.body = 'hello world'
})

app.listen(59911, () => {
  console.log('koa server is starting at http://localhost:59911')
})
