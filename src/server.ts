/***************************************************
 * Created by nanyuantingfeng on 2022/4/3 18:15. *
 ***************************************************/
import { LWM2MServer } from '@hollowy/lwm2m'

const lwm2m = new LWM2MServer({ port: 59999 })

lwm2m.on('error', (err) => {
  console.error(err)
})

lwm2m.on('device::status', (cnode, status) => {
  console.log('status', cnode.clientName, status)
})

lwm2m.on('device::leaving', (clientName: string, mac: string) => {
  console.log('leaving', clientName, mac)
})

lwm2m.on('device::incoming', async (cnode, data: any) => {
  console.log('incoming', cnode.clientName)
  await cnode.observe('/19/0/0')
})

lwm2m.on('device::notify', (cnode, data: any) => {
  console.log('notify', cnode.clientName, JSON.stringify(data))
})

lwm2m.on('error', (err) => {
  throw err
})

lwm2m.on('ready', () => {
  console.log('>> LWM2M Server started!')
  lwm2m.shepherd.alwaysPermitJoin(true)
})

lwm2m.start((err) => {
  if (err) return console.error(err)
})
